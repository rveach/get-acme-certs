import hashlib
import os
import re
import sys
import tempfile

import pytest

sys.path.append(
    os.path.join(
        os.path.split(
            os.path.dirname(
                os.path.abspath(__file__)
            )
        )[0],
        "src",
    )
)

from get_acme_certs import main

def get_fp(filename):
    return os.path.join(os.path.dirname(os.path.abspath(__file__)), "certs", filename)

def hash_cert_contents(filename):

    pattern = re.compile(r"\-{5}BEGIN\sCERTIFICATE\-{5}([\s\S]+)\-{5}END\sCERTIFICATE\-{5}")

    with open(filename, "r") as fd:
        match = re.search(pattern, fd.read())

    if match:
        hashlib.sha256(bytes(match.groups()[0].strip(), 'utf-8')).hexdigest()
    else:
      return None
        
def certs_match(file1, file2):
    return (hash_cert_contents(file1) == hash_cert_contents(file2))


def hash_file(the_file):
    with open(the_file, "rb") as f:
        the_hash = hashlib.sha256(f.read()).hexdigest()
    return the_hash

def files_match(file1, file2):
    return (hash_file(file1) == hash_file(file2))

@pytest.fixture
def sample_files():

    sample_files = {
        "acme": get_fp("test_acme.json"),
        "cert": get_fp("test_cert.pem"),
        "chain": get_fp("test_chain.pem"),
        "fullchain": get_fp("test_fullchain.pem"),
        "privkey": get_fp("test_privkey.pem"),
    }

    return sample_files


def test_files_exist(sample_files):

    for key in sample_files.keys():
        assert os.path.exists(sample_files[key])


def test_extraction(sample_files):

    with tempfile.TemporaryDirectory() as extractdir:
        main(sample_files['acme'], 'rveach', extractdir)
        assert os.path.exists(os.path.join(extractdir, 'privkey.pem'))
        assert os.path.exists(os.path.join(extractdir, 'fullchain.pem'))
        assert os.path.exists(os.path.join(extractdir, 'cert.pem'))
        assert os.path.exists(os.path.join(extractdir, 'chain.pem'))

        assert files_match(os.path.join(extractdir, 'privkey.pem'), sample_files['privkey'])
        assert files_match(os.path.join(extractdir, 'fullchain.pem'), sample_files['fullchain'])
        assert certs_match(os.path.join(extractdir, 'chain.pem'), sample_files['chain'])
        assert certs_match(os.path.join(extractdir, 'cert.pem'), sample_files['cert'])
