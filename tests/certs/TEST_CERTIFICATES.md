# Test Certificates

## Files

- `test_acme.json` - Sample acme.json file to use for testing.
- `test_cert.pem` - Certificate
- `test_chain.pem` - CSR
- `test_fullchain.pem` - Certificate and CSR
- `test_privkey.pem` - Private Key

## Test Process

1. Extract fields from `test_acme.json`
2. Locate the domain with the name rveach in json.
3. Create `fullchain.pem` from base64 decoded `Certificate` field.
4. Create `privkey.pem` from base64 decoded `Key` field.  This should match `test_privkey.pem`.
5. Split top half of `fullchain.pem` off into `cert.pem`, which should match `test_cert.pem`.
6. Split bottom half of `fullchain.pem` off into `chain.pem`, which should match `test_chain.pem`.

### How these files are used with OpenLDAP

```
# Certificate/SSL Section
TLSCipherSuite DEFAULT
TLSCertificateFile /etc/letsencrypt/live/ldap.my-domain.com/cert.pem
TLSCertificateKeyFile /etc/letsencrypt/live/ldap.my-domain.com/privkey.pem
TLSCACertificateFile /etc/letsencrypt/live/ldap.my-domain.com/chain.pem
TLSCACertificatePath /usr/share/ca-certificates/trust-source
```

## certificate.pem

### Command used to create

```bash
openssl req -newkey rsa:2048 -nodes -keyout key.pem -x509 -out certificate.pem
```

### Command used to dump info:

```bash
openssl x509 -text -noout -in certificate.pem
```

### Friendly Output

```
Certificate:
    Data:
        Version: 3 (0x2)
        Serial Number: 13127650533263178190 (0xb62ec89b529e79ce)
    Signature Algorithm: sha256WithRSAEncryption
        Issuer: C=US, ST=Missouri, L=Kansas CIty, O=rveach, OU=rveach, CN=rveach/emailAddress=email@gitlab.com
        Validity
            Not Before: Feb  2 04:44:19 2019 GMT
            Not After : Mar  4 04:44:19 2019 GMT
        Subject: C=US, ST=Missouri, L=Kansas CIty, O=rveach, OU=rveach, CN=rveach/emailAddress=email@gitlab.com
        Subject Public Key Info:
            Public Key Algorithm: rsaEncryption
                Public-Key: (2048 bit)
                Modulus:
                    00:c1:0b:00:00:3b:63:3b:af:37:1a:a8:95:07:c9:
                    d2:e1:c6:a6:7b:7c:69:da:bd:be:1e:3f:ff:c5:38:
                    8b:29:a1:5d:15:6e:7d:f4:dd:fe:2e:bc:43:db:fb:
                    e3:b7:16:00:be:a1:66:c6:81:67:26:32:5b:92:ea:
                    e0:52:31:15:4f:e6:79:ee:13:12:fb:2f:63:88:a0:
                    3f:b2:e2:12:f4:c7:c4:25:a0:f2:3e:84:70:ec:8c:
                    b3:f5:85:30:12:22:b7:0c:29:1d:80:5a:f1:73:da:
                    d5:c2:d0:01:20:ab:1a:60:a0:43:34:95:30:63:a6:
                    6e:c0:c3:dd:96:c5:7c:6a:d6:7a:6e:46:cf:74:f7:
                    bb:fe:1f:66:b6:92:09:04:1e:63:8b:0e:b6:f1:b7:
                    99:5a:1f:28:ab:51:0d:1e:1b:85:2a:2e:cb:2e:5d:
                    0a:e6:49:42:2e:5e:ec:e0:84:5b:59:84:3b:66:6c:
                    e8:7f:72:f1:a2:2a:ed:4a:2e:67:e2:35:c5:04:ca:
                    94:5e:90:b4:7a:61:91:78:6e:b2:7f:6f:af:cf:4a:
                    c7:19:4e:6d:72:73:7f:cb:1a:22:af:ea:5d:32:c4:
                    e9:57:b3:10:24:f5:1b:ac:b1:ce:56:8f:0f:f2:ee:
                    25:9a:09:5a:fa:d9:c6:4d:87:f8:b4:d0:b2:4f:4a:
                    35:5d
                Exponent: 65537 (0x10001)
        X509v3 extensions:
            X509v3 Subject Key Identifier:
                EB:E8:A2:48:0C:1C:BC:33:16:D0:92:B6:E4:BE:42:37:84:F1:25:E4
            X509v3 Authority Key Identifier:
                keyid:EB:E8:A2:48:0C:1C:BC:33:16:D0:92:B6:E4:BE:42:37:84:F1:25:E4

            X509v3 Basic Constraints:
                CA:TRUE
    Signature Algorithm: sha256WithRSAEncryption
         16:5f:4d:ae:04:4b:0f:3e:37:c4:1e:fa:56:ea:42:86:9c:79:
         b4:55:2e:47:08:60:06:24:3b:f3:ca:a1:8e:b0:08:16:6c:fd:
         1c:83:2b:94:56:cf:4d:aa:35:93:9c:31:37:de:c5:7e:b9:e3:
         42:ce:df:c7:13:66:ab:3a:81:18:38:e6:48:9d:d1:c5:8f:c0:
         34:36:d7:4b:56:a0:94:da:02:6c:96:01:53:d6:20:9f:30:f6:
         4a:6c:b9:a6:05:9b:02:6e:34:d0:ce:08:ee:da:e4:d5:45:3b:
         da:47:14:72:f6:b8:e9:08:c6:b8:4b:cc:45:57:85:ef:6f:4b:
         1f:a1:3b:de:b1:6a:c4:90:e3:c7:bc:cf:f4:d8:e5:61:8f:68:
         59:46:f9:af:cd:82:67:fc:80:9a:12:4c:18:49:8d:43:5f:75:
         9f:30:8e:7e:0c:94:c6:00:c2:a8:fd:a4:c0:8a:03:da:93:42:
         79:98:3f:dc:c2:53:65:c7:8d:ed:7a:46:33:28:6c:e9:43:c0:
         76:67:47:80:b4:5f:ef:3a:08:35:da:3c:ec:35:ce:f5:13:ce:
         36:fb:e6:b7:65:cc:44:5a:51:bf:90:d5:bc:a2:b3:aa:f3:7e:
         34:4c:0e:f1:02:43:39:a2:ae:d5:af:e9:40:e5:32:0a:83:29:
         63:11:88:af
```
